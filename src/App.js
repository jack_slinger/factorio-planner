import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import './App.scss';

import Factory from './features/factory/Factory';
import Section from 'react-bulma-components/lib/components/section';
import Container from 'react-bulma-components/lib/components/container';
// import { Button } from 'react-bulma-components';

function App() {
  return (
    <Section>
      <Container>
        <Factory />
      </Container>
    </Section>
  );
}

export default App;
