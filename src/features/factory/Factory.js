import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  addLine,
  removeLine,
  selectLines,
} from './factorySlice';

import recipes from '../../data/angel_bobs_recipes.json'

import { Table, Box, Button } from 'react-bulma-components';
import { Field, Control, Label, Input } from 'react-bulma-components/lib/components/form';
import Select from 'react-select';
import AssemblyLine from './assembly_line/AssemblyLine';

const Factory = () => {
  const lines = useSelector(selectLines);
  const dispatch = useDispatch();
  const options = Object.entries(recipes).map(([key, recipe], index) => { return { value: key, label: key } } );

  return (
    <>
      <Box>
        <h2 className="title">Assembly lines</h2>
        <Table>
          <thead>
            <tr>
              <th colSpan="3">Inputs</th>
              <th colSpan="3">Outputs</th>
              <th colSpan="5"></th>
            </tr>
            <tr>
              <th>Name</th>
              <th>Amount</th>
              <th>per second</th>
              <th>Name</th>
              <th>Amount</th>
              <th>per second</th>
              <th>Process time</th>
              <th>Factory speed</th>
              <th>Real time</th>
              <th>Number of machines</th>
              <th></th>
            </tr>
          </thead>
          {lines.map((line, i) => {
            return(
              <AssemblyLine
                key={i}
                inputs={line.inputs}
                outputs={line.outputs}
                processTime={line.processTime}
                factorySpeed={line.factorySpeed}
                handleRemoval={() => dispatch(removeLine(i))}
              />
            )
          })}
        </Table>
      </Box>
      <Box>
        <h3 className="subtitle">Add new Assembly Line</h3>
        <Field>
          <Label>Recipe:</Label>
          <Control>
            <Select
              options={options}
              placeholder="Select a recipe..."
            />
          </Control>
        </Field>
        <Field>
          <Label>Machine:</Label>
          <Control>
            <Input />
          </Control>
        </Field>
        <Button color="primary">Add</Button>
      </Box>
    </>
  )
}

export default Factory