import React, { useState } from 'react';

const AssemblyLine = ({ inputs, outputs, processTime, factorySpeed, handleRemoval }) => {
  const [numOfMachines, setnumOfMachines] = useState(1);
  
  const realTime = (processTime / factorySpeed).toFixed(2);

  let rows;
  if (inputs.length > outputs.length) {
    rows = inputs.map((input, i) => {
      return {
        input: input,
        output: outputs[i] || {}
      }
    });
  } else {
    rows = outputs.map((output, i) => {
      return {
        input: inputs[i] || {},
        output: output
      }
    });
  }

  return (
    <tbody className="no-border">
      {rows.map((row, i) => {
        return (
          <tr key={i}>
            <td>{row.input.name}</td>
            <td>{row.input.amount}</td>
            <td>
              <strong>{row.input.amount && ((row.input.amount / realTime) * numOfMachines).toFixed(2)}</strong>
            </td>
            <td>{row.output.name}</td>
            <td>{row.output.amount}</td>
            <td>
              <strong>{row.output.amount && ((row.output.amount / realTime) * numOfMachines).toFixed(2)}</strong>
            </td>
            {i === 0 &&
              <>
                <td rowSpan={rows.length}>{processTime}</td>
                <td rowSpan={rows.length}>{factorySpeed}</td>
                <td rowSpan={rows.length}>{realTime}</td>
                <td rowSpan={rows.length}>
                  <input
                    value={numOfMachines}
                    onChange={(e) => { setnumOfMachines(e.target.value) }}
                  />
                </td>
                <td rowSpan={rows.length}>
                  <button
                    onClick={handleRemoval}
                  >
                    -
                  </button>
                </td>
              </>
            }
          </tr>
        )
      })}
    </tbody>
  )
}

export default AssemblyLine