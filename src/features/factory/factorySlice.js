import { createSlice } from '@reduxjs/toolkit';

export const FactorySlice = createSlice({
  name: 'factory',
  initialState: {
    lines: [
      {
        inputs: [
          { name: 'Hybrid Catalyst', amount: 1 },
          { name: 'Saphirite Crystals', amount: 2 },
          { name: 'Crotinnium Crystals', amount: 2 },
          { name: 'Bomonium Crystals', amount: 2 }
        ],
        outputs: [
          { name: 'Uranium Ore', amount: 3 }
        ],
        processTime: 1.5,
        factorySpeed: 1
      },
      {
        inputs: [
          { name: 'Test', amount: 1 }
        ],
        outputs: [
          { name: 'Test', amount: 3 }
        ],
        processTime: 1.75,
        factorySpeed: 1
      },
      {
        inputs: [
          { name: 'Cow', amount: 1 }
        ],
        outputs: [
          { name: 'Cow', amount: 3 }
        ],
        processTime: 1.75,
        factorySpeed: 1
      }
    ]
  },
  reducers: {
    addLine: (state, action) => {
      state.lines += action.payload;
    },
    removeLine: (state, action) => {
      state.lines.splice(action.payload, 1);
    }
  },
});

export const { addLine, removeLine } = FactorySlice.actions;

export const selectLines = state => state.factory.lines;

export default FactorySlice.reducer;