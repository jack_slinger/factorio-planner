import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
import factoryReducer from '../features/factory/factorySlice';

export default configureStore({
  reducer: {
    counter: counterReducer,
    factory: factoryReducer
  },
});
